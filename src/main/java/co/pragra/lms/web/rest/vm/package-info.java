/**
 * View Models used by Spring MVC REST controllers.
 */
package co.pragra.lms.web.rest.vm;
