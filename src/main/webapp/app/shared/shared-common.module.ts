import { NgModule } from '@angular/core';

import { PlmsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [PlmsSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [PlmsSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class PlmsSharedCommonModule {}
