import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PlmsSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { ServicesComponent } from './services/services.component';
import { PartnersComponent } from './partners/partners.component';
import { ProgramCardsComponent } from './program-cards/program-cards.component';
import { ProgramCardComponent } from './program-card/program-card.component';

@NgModule({
    imports: [PlmsSharedModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent, ServicesComponent, PartnersComponent, ProgramCardsComponent, ProgramCardComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlmsHomeModule {}
