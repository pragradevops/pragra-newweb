import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'jhi-program-card',
  templateUrl: './program-card.component.html',
  styles: []
})
export class ProgramCardComponent implements OnInit {

    @Input()
    card: { name: string , description: string , duration: string , price: string , imgsrc: string };

  constructor() { }

  ngOnInit() {
  }

}
